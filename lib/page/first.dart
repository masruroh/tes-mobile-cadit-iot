import 'package:flutter/material.dart';

import 'general.dart';

class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  String input = '0', output = '';
  processOutput() {
    String temp = '', ordinal = '';
    int digit = int.parse(input);
    for (var i = 1; i <= digit; i++) {
      temp += i.toString();
      if (temp.length >= digit) {
        setState(() {
          if (input.substring(input.length - 1) == '1' && digit != 11) {
            ordinal = 'st';
          } else if (input.substring(input.length - 1) == '2' && digit != 12) {
            ordinal = 'nd';
          } else {
            ordinal = 'th';
          }
          output = input +
              ordinal +
              ' digit number is ' +
              temp[digit - 1] +
              ' lies on number ' +
              i.toString();
        });
        break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bg,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(jarak),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                field('Input:', (value) => setState(() => input = value),
                    num: true, hint: 'Digit'),
                SizedBox(height: jarak),
                MaterialButton(
                    onPressed: () => processOutput(),
                    child: Text('Submit'),
                    color: putih),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Output', style: style(size[1], b: false)),
                    Container(
                      width: double.infinity,
                      height: 300,
                      padding: EdgeInsets.fromLTRB(jarak, 0, jarak, 0),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(rad),
                          border: Border.all(),
                          color: putih),
                      child: Center(
                          child: Text(output, style: style(size[1], b: false))),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
