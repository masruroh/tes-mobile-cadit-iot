import 'package:flutter/material.dart';

double jarak = 10, rad = 5;
Color putih = Colors.white,
    hitam = Colors.black,
    bg = Color(0XFFe6edf1),
    bg2 = Color(0XFF2c4260),
    bg3 = Color(0XFF808d9f);
List<double> size = [15, 17, 20];
style(double size, {Color color = Colors.black, bool b = false}) {
  return TextStyle(
      fontSize: size,
      color: color,
      fontWeight: (b) ? FontWeight.bold : FontWeight.normal);
}

// UNTUK LOADING
loading() {
  return SafeArea(
      child: Scaffold(
          backgroundColor: bg,
          body: Center(
            child: Container(
                width: 150,
                child: Image.asset('assets/loading.gif', fit: BoxFit.cover)),
          )));
}

field(String title, Function onchange,
    {TextEditingController tec,
    bool num = false,
    Color color = Colors.white,
    bool b = false,
    String hint = ''}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(title, style: style(size[1], b: b)),
      Container(
        padding: EdgeInsets.fromLTRB(jarak, 0, jarak, 0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(rad),
            border: Border.all(),
            color: color),
        child: TextFormField(
          controller: tec,
          onChanged: onchange,
          keyboardType: (num) ? TextInputType.number : TextInputType.text,
          decoration: InputDecoration(border: InputBorder.none, hintText: hint),
        ),
      ),
      SizedBox(height: jarak),
    ],
  );
}
