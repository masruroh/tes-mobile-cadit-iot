import 'package:flutter/material.dart';

import 'general.dart';
import 'first.dart';
import 'task2/second.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  contData(String title, {Function function}) {
    return GestureDetector(
      onTap: function,
      child: Container(
        width: 100,
        padding: EdgeInsets.all(jarak),
        decoration: BoxDecoration(
            color: hitam, borderRadius: BorderRadius.circular(rad)),
        child: Text(title,
            style: TextStyle(color: putih), textAlign: TextAlign.center),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.infinity,
          padding: EdgeInsets.all(jarak * 2),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              contData(
                'Task 1',
                function: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FirstPage()));
                },
              ),
              SizedBox(height: jarak * 2),
              contData(
                'Task 2',
                function: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SecondPage()));
                },
              ),
              SizedBox(height: jarak * 2),
              Text('Create by : Izzatul Masruroh'),
            ],
          ),
        ),
      ),
    );
  }
}
