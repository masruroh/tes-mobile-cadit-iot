import 'package:flutter/material.dart';

import '../general.dart';

class WidgetDaily extends StatefulWidget {
  const WidgetDaily({this.data, this.title});
  final List<TableRow> data;
  final String title;
  @override
  _WidgetDailyState createState() => _WidgetDailyState();
}

class _WidgetDailyState extends State<WidgetDaily> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Color(0xff2c4260), borderRadius: BorderRadius.circular(rad)),
      padding: EdgeInsets.all(jarak * 2),
      margin: EdgeInsets.all(jarak),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(width: 38),
              Text('Daily', style: style(22, color: Colors.white)),
              SizedBox(width: 4),
              Text((widget.title == null) ? '' : widget.title,
                  style: style(16, color: Color(0xff77839a))),
            ],
          ),
          SizedBox(height: jarak),
          Flexible(
            child: SingleChildScrollView(
              child: Table(
                  border: TableBorder.all(color: bg3, width: 2),
                  defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                  children: widget.data),
            ),
          ),
        ],
      ),
    );
  }
}
