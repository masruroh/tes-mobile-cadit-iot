import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import '../general.dart';

class WidgetHourly extends StatefulWidget {
  const WidgetHourly({this.data, this.title});
  final List<BarChartGroupData> data;
  final String title;
  @override
  _WidgetHourlyState createState() => _WidgetHourlyState();
}

class _WidgetHourlyState extends State<WidgetHourly> {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        color: bg2,
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 38),
                  Text('Hourly', style: style(22, color: Colors.white)),
                  SizedBox(width: 4),
                  Text((widget.title == null) ? '' : widget.title,
                      style: style(16, color: Color(0xff77839a))),
                ],
              ),
              SizedBox(height: 38),
              Expanded(
                child: BarChart(
                  BarChartData(
                    maxY: (widget.title == 'Temperature')
                        ? 400
                        : (widget.title == 'Humidity')
                            ? 110
                            : (widget.title == 'Barometer')
                                ? 1200
                                : 0,
                    barTouchData: BarTouchData(
                      touchTooltipData: BarTouchTooltipData(
                        tooltipBgColor: Colors.grey,
                        getTooltipItem: (_a, _b, _c, _d) => null,
                      ),
                    ),
                    titlesData: FlTitlesData(
                      show: true,
                      rightTitles: SideTitles(showTitles: false),
                      topTitles: SideTitles(showTitles: false),
                      bottomTitles: SideTitles(
                        showTitles: true,
                        getTextStyles: (context, value) =>
                            style(14, color: bg3, b: true),
                        margin: 5,
                        rotateAngle: 75,
                        getTitles: (double value) {
                          if (value % 4 == 0) {
                            return value.toInt().toString() + ':00';
                          } else {
                            return '';
                          }
                        },
                      ),
                      leftTitles: SideTitles(
                        showTitles: true,
                        getTextStyles: (context, value) =>
                            style(14, color: bg3, b: true),
                        margin: 8,
                        reservedSize: 28,
                        interval: 1,
                        getTitles: (value) {
                          String val = '';
                          if (widget.title == 'Temperature') {
                            if (value % 100 == 0) {
                              val = value.toInt().toString() + ' F';
                            } else {
                              val = '';
                            }
                          } else if (widget.title == 'Humidity') {
                            if (value % 10 == 0) {
                              val = value.toInt().toString();
                            } else {
                              val = '';
                            }
                          } else {
                            if (value % 300 == 0) {
                              val = value.toInt().toString();
                            } else {
                              val = '';
                            }
                          }
                          return val;
                        },
                      ),
                    ),
                    borderData: FlBorderData(show: false),
                    barGroups: (widget.data == null) ? [] : widget.data,
                    gridData: FlGridData(show: false),
                  ),
                ),
              ),
              SizedBox(height: 12),
            ],
          ),
        ),
      ),
    );
  }
}
