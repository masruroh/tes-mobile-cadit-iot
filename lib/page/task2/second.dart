import 'dart:convert';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../general.dart';
import 'daily.dart';
import 'hourly.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  int val = 0; // 0 temp, 1 hum, 2 baro
  List<TableRow> listDailyTemp = [], listDailyHum = [], listDailyBaro = [];
  List<BarChartGroupData> listHourlyTemp = [],
      listHourlyHum = [],
      listHourlyBaro = [];
  bool get = false;

  makeGroupData(int x, double y1) {
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(y: y1, colors: [Color(0xff53fdd7)], width: 7),
    ]);
  }

  getData() async {
    print('get data');
    final String response = await rootBundle.loadString('assets/data.json');
    final data = await json.decode(response);
    setState(() {
      get = false;
      listDailyTemp = [];
      listHourlyTemp = [];
      listDailyHum = [];
      listHourlyHum = [];
      listDailyBaro = [];
      listHourlyBaro = [];
    });
    var tempList = data['data'];
    TableRow judul = TableRow(children: [
      valCell('NO', center: true),
      valCell('Date', center: true),
      valCell('Value', center: true),
    ]);
    listDailyTemp.add(judul);
    listDailyHum.add(judul);
    listDailyBaro.add(judul);
    for (int i = 0; i < tempList.length; i++) {
      // for (int i = 0; i < 10; i++) {
      var temp = tempList[i];
      // temperature
      print(temp['date']);
      listDailyTemp.add(
        TableRow(children: [
          valCell((i + 1).toString()),
          valCell(temp['date'].toString()),
          valCell(temp['main']['temp'].toString()),
        ]),
      );
      listHourlyTemp.add(makeGroupData(i, temp['main']['temp'].toDouble()));

      // humidity
      listDailyHum.add(
        TableRow(children: [
          valCell((i + 1).toString()),
          valCell(temp['date'].toString()),
          valCell(temp['main']['humidity'].toString()),
        ]),
      );
      listHourlyHum.add(makeGroupData(i, temp['main']['humidity'].toDouble()));

      // barometer
      listDailyBaro.add(
        TableRow(children: [
          valCell((i + 1).toString()),
          valCell(temp['date'].toString()),
          valCell(temp['main']['pressure'].toString()),
        ]),
      );
      listHourlyBaro.add(makeGroupData(i, temp['main']['pressure'].toDouble()));
    }
    setState(() => get = true);
  }

  valCell(String text, {bool center = false}) {
    return TableCell(
        verticalAlignment: TableCellVerticalAlignment.middle,
        child: Container(
          padding: EdgeInsets.all(jarak),
          child: Text(text,
              style: style(13, color: putih),
              textAlign: (center) ? TextAlign.center : TextAlign.left),
        ));
  }

  _decide() {
    return DefaultTabController(
      length: 3,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: bg3,
            title: TabBar(
              tabs: [
                Tab(
                  child: Text('Temperature',
                      style: style(size[0], color: hitam, b: true)),
                ),
                Tab(
                  child: Text('Humidity',
                      style: style(size[0], color: hitam, b: true)),
                ),
                Tab(
                  child: Text('Barometer',
                      style: style(size[0], color: hitam, b: true)),
                ),
              ],
              indicatorColor: hitam,
            ),
            automaticallyImplyLeading: false,
          ),
          bottomNavigationBar: Container(
            decoration: BoxDecoration(
              color: bg3,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(rad * 5),
                  topRight: Radius.circular(rad * 5)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                    icon: Icon(Icons.refresh), onPressed: () => getData()),
                SizedBox(width: jarak),
                IconButton(
                    icon: Icon(Icons.exit_to_app),
                    onPressed: () => Navigator.of(context).pop()),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Column(
                children: [
                  SizedBox(height: jarak),
                  Flexible(
                      child: WidgetHourly(
                          title: 'Temperature', data: listHourlyTemp)),
                  Flexible(
                      child: WidgetDaily(
                          title: 'Temperature', data: listDailyTemp)),
                ],
              ),
              Column(
                children: [
                  Flexible(
                      child:
                          WidgetHourly(title: 'Humidity', data: listHourlyHum)),
                  Flexible(
                      child:
                          WidgetDaily(title: 'Humidity', data: listDailyHum)),
                ],
              ),
              Column(
                children: [
                  SizedBox(height: jarak),
                  Flexible(
                      child: WidgetHourly(
                          title: 'Barometer', data: listHourlyBaro)),
                  Flexible(
                      child:
                          WidgetDaily(title: 'Barometer', data: listDailyBaro)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    getData();
  }

  @override
  Widget build(BuildContext context) {
    return (get) ? _decide() : loading();
  }
}
